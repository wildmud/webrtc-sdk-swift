//
//  ViewController.swift
//  WRKit
//
//  Created by sugarfifty.vipservice@gmail.com on 01/10/2019.
//  Copyright (c) 2019 sugarfifty.vipservice@gmail.com. All rights reserved.
//

import UIKit
import WRKit

class ViewController: UIViewController {
    
    var client = WRClient(userID:"3345678")
    
    @IBOutlet weak var remoteView: UIView!
    
    @IBOutlet weak var localView: UIView!
    
    @IBOutlet weak var hangupBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.localView.addSubview(self.client.localVideoView)
        self.remoteView.addSubview(self.client.remoteVideoView)
        self.client.remoteVideoView.translatesAutoresizingMaskIntoConstraints = false
        self.client.remoteVideoView.topAnchor.constraint(equalTo: self.remoteView.topAnchor, constant: 0).isActive = true
        self.client.remoteVideoView.bottomAnchor.constraint(equalTo: self.remoteView.bottomAnchor, constant: 0).isActive = true
        self.client.remoteVideoView.leadingAnchor.constraint(equalTo: self.remoteView.leadingAnchor, constant: 0).isActive = true
        self.client.remoteVideoView.trailingAnchor.constraint(equalTo: self.remoteView.trailingAnchor, constant: 0).isActive = true
        
        self.client.onEvent = { (event:WRClientEvent) in
            if event == .connected
            {
                print("event connected")
            }
            
            if event == .joinedRoom
            {
                print("event joinedRoom")
            }
            
            if event == .leaveRoom
            {
                print("event leaveRoom")
            }
            
            if event == .localVideoSizeChange
            {
                print("本地長寬變更")
                print("localVideoSizeChange", self.client.localVideoSize)
            }
            
            if event == .streamingSizeChange
            {
                print("串流長寬變更，等同android onStreaming")
                print("Self Size", self.client.localVideoSize)
                print("Remote Size",self.client.remoteVideoView.videoSize)
            }
        }
        let alert = UIAlertController(title: "onMessage", message: nil, preferredStyle: .alert)
        self.client.onMessage = {(senderID:String, message:String, type:String) in
            print ("onMessage:\(message)")
            alert.title = message
            if self.presentedViewController != nil
            {
                //                alert.dismiss(animated: true, completion: {
                //                    self.present(alert, animated: true, completion: nil)
                //                })
            }
            else
            {
                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
        
        // 預設是開Video的room
        // client.join(self.randomRoomID(), type:.video)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.client.localVideoView.frame  = self.localView.bounds
        
        self.client.remoteVideoView.frame = self.remoteView.bounds
    }
    
    @IBAction func hangup(_ sender: Any) {
        //拒接 or 掛斷
        self.client.leave()
    }
    var messageNo = 0
    @IBAction func sendMessage(_ sender: Any) {
        self.messageNo = self.messageNo + 1
        self.client.send(userID: "33456789", message: "HELLO\(self.messageNo)")
    }
    
    @IBAction func videoCall(_ sender: Any)
    {
        client.remove("3345678")
        {   (status) in
            
            if status == false { return print ("remove room failed") }
            
            self.client.join("3345678", type:.video)
            
        }
    }
    
    @IBAction func audioCall(_ sender: Any)
    {
        client.remove("3345678")
        {   (status) in
            
            if status == false { return print ("remove room failed") }
            
            self.client.join("3345678", type:.audio)
            
        }
    }
    
    @IBAction func joinRoom(_ sender: Any)
    {
        self.client.join("3345678", type:.video)
    }
    
    @IBAction func openCamera(_ sender: Any)
    {
        client.openCamera()
    }
    
    @IBAction func closeCamera(_ sender: Any)
    {
        client.closeCamera()
    }
    
    @IBAction func changeCameraSize(_ sender: Any)
    {
        client.localVideoSize = (client.localVideoSize == CGSize(width: 1920, height: 1440)) ?  CGSize(width: 960, height: 540) : CGSize(width: 1920, height: 1440)
    }
    
    @IBAction func switchCamera(_ sender: Any)
    {
        client.switchCamera()
    }
    
    @IBAction func switchAudio(_ sender: Any)
    {
        client.audioOutputType = client.audioOutputType == .speaker ? .headphone : .speaker
    }
    
    private func randomRoomID() -> String
    {
        let letters : NSString = "0123456789"
        
        let len = UInt32(letters.length)
        
        var roomID = ""
        
        for _ in 0 ..< 9 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            roomID += NSString(characters: &nextChar, length: 1) as String
        }
        return roomID
    }
}
