//
//  AppDelegate.swift
//  WRKit
//
//  Created by sugarfifty.vipservice@gmail.com on 01/10/2019.
//  Copyright (c) 2019 sugarfifty.vipservice@gmail.com. All rights reserved.
//

import UIKit
import WRKit
import PushKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        WRSDK.launch()
        
        WRConfig.shared.roomServerHost = "https://webrtc.sunlyc.com"
        
        WRConfig.shared.messageHost = "https://dev.sunlyc.com:6012"
        
        WRConfig.shared.ApiHost = "https://dev.sunlyc.com"
        
        WRConfig.shared.ringTonePath = Bundle.main.path(forResource: "ring", ofType: "mp3")
        
        self.voipRegistration()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        self.voipRegistration()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }

    func voipRegistration() {
        
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: DispatchQueue.main)
        
        voipRegistry.delegate = self
        
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print ("granted", granted)
            if let error = error
            {
                print ("error", error)
            }
        }
    }
}

extension AppDelegate : PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        
        //新增or刪除device Token使用以下方法都可
        WRSDK.registerDeviceToken("3345678", pushCredentials: pushCredentials)
        
        //let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        
        //NSLog("TOKEN:%@", token)
        
        //WRSDK.registerDeviceToken("3345678", token: token)
        
        //WRSDK.removeDeviceToken("3345678", pushCredentials: pushCredentials)
        
        //WRSDK.removeDeviceToken("3345678", token: token)
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        NSLog("didReceiveIncomingPushWith:%@", payload.dictionaryPayload)
        
    }
}
