# WRKit

[![CI Status](https://img.shields.io/travis/sugarfifty.vipservice@gmail.com/WRKit.svg?style=flat)](https://travis-ci.org/sugarfifty.vipservice@gmail.com/WRKit)
[![Version](https://img.shields.io/cocoapods/v/WRKit.svg?style=flat)](https://cocoapods.org/pods/WRKit)
[![License](https://img.shields.io/cocoapods/l/WRKit.svg?style=flat)](https://cocoapods.org/pods/WRKit)
[![Platform](https://img.shields.io/cocoapods/p/WRKit.svg?style=flat)](https://cocoapods.org/pods/WRKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WRKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WRKit'
```

## Author

aleiku@gmail.com

## License

WRKit is available under the MIT license. See the LICENSE file for more info.
