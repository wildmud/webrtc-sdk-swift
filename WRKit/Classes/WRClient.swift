//
//  WRClient.swift
//  webrtc-demo
//
//  Created by Aleiku on 2018/10/23.
//  Copyright © 2018年 Institute for Information Industry. All rights reserved.
//

import UIKit

struct WRMessage: Decodable {
    let type: String
    let from: Int
    let to: Int
    let message: String
}

enum WRClientState  {
    case disconnected
    case connecting
    case connected
}

public enum WRRoomType  {
    case video
    case audio
}

public enum WRClientEvent {
    case connected
    case joinedRoom
    case leaveRoom
    case localVideoSizeChange
    case streamingSizeChange
}

public enum WRAudioOutputType  {
    case headphone
    case speaker
}

open class WRClient: NSObject {
    
    private var client = ARDAppClient()
    public  var onEvent:((WRClientEvent) -> Void) = {(event) in }
    public  var onMessage:((String, String, String) -> Void) = {(senderID, msg, eventType) in }
    public  var userID:String = ""
    public  var roomID:String = ""
    public  var roomType:WRRoomType = .video
    public  var audioOutputType:WRAudioOutputType = .speaker
    {
        didSet {
            if oldValue == audioOutputType { return }
            
            self.configureAudioSession()
        }
    }
    // 用來設定擷取畫面的視訊大小
    public var localVideoSize:CGSize = CGSize(width: 1920, height: 1080) {
        didSet {
            if __CGSizeEqualToSize(oldValue, localVideoSize) { return }
            
            self.configureVideoSession()
            
            self.onEvent(.localVideoSizeChange)
        }
    }
    public  var localVideoView:RTCCameraPreviewView = RTCCameraPreviewView(frame: .zero)
    public  var remoteVideoView = WRRemoteVideoView(frame:.zero)
    private var localCapturer:RTCCameraVideoCapturer?
    private var captureController:ARDCaptureController?
    private var _remoteVideoSourceView:(UIView & RTCVideoRenderer)!
    private var echo:Echo = Echo(options: [String : Any]())
    private var _audioPlayer:AVAudioPlayer?
    
    var remoteVideoTrack:RTCVideoTrack? {
        willSet{
            self.remoteVideoTrack?.remove(self._remoteVideoSourceView)
        }
        didSet{
            self.remoteVideoTrack?.add(self._remoteVideoSourceView)
        }
    }
    
    public override init() {
        
        #if RTC_SUPPORTS_METAL
            _remoteVideoView = RTCMTLVideoView(frame: .zero)
        #else
            let remoteView = RTCEAGLVideoView(frame: .zero)
            _remoteVideoSourceView = remoteView
        #endif
        
        super.init()
        
        if let remoteView = self._remoteVideoSourceView as? RTCEAGLVideoView
        {
            remoteView.delegate = self as! RTCVideoViewDelegate
        }
        
        self.remoteVideoView.delegate = self as WRRemoteVideoViewDelegate
        
        self._remoteVideoSourceView = self.remoteVideoView._videoView
        
    }
    
    // 帶入UserID
    public convenience init(userID:String)
    {
        self.init()
        self.userID  = userID
        commonInit()
    }
    
    
    convenience init(userID:String, _ callback:@escaping ((WRClientEvent ) -> Void)) {
        self.init()
        self.userID  = userID
        self.onEvent = callback
    }
    
    // COMMON INIT
    // 掛上Laravel Echo
    // 掛上 Channel： "chat-room"
    // Listen Event： ".webrtc.messaging"
    func commonInit()
    {
        
        self.echo = Echo(options: [
            "host": WRConfig.shared.messageHost,
            "auth": ["headers": ["Authorization": "Bearer \(self.roomID)"]]
            ])
        
        self.echo.connected(){ data, ack in
            
            self.echo.channel(channel: "chat-room").listen(event: ".webrtc.messaging", callback: { data, ack in
                
                if data.count >= 2
                    , let event = data[1] as? NSDictionary
                    , let event_type = event["type"] as? String
                    // , event_type == "msg"
                    , let message = event["message"] as? String
                    , let to = event["to"] as? String
                    , to == self.userID
                    , let from = event["from"] as? String
                {
                    self.onMessage(from, message, event_type)
                }
            })
            
        }
    }
    
    // JOIN ROOM
    // 轉call
    // func public func join(_ roomID:String, type:.video)
    public func join(_ roomID:String) {
        
        self.join(roomID, type: .video)
    }
    
    // JOIN ROOM
    // 帶入房號ID與ROOM SERVER溝通
    // 可選video chat or voice chat
    // 若有設定WRConfig.shared.ringTonePath
    // 會播放tone path鈴聲直至另一人加入room
    public func join(_ roomID:String, type:WRRoomType) {
        
        self.roomID = roomID
        
        self.roomType = type
        
        self.audioOutputType = (type == .video) ? .speaker : .headphone
        
        ARDAppEngineClient.roomHost = WRConfig.shared.roomServerHost
        
        self.client = ARDAppClient(delegate: self)
        
        self.client.offerToReceiveAudio = true
        
        self.client.offerToReceiveVideo = type == .video
        
        self.client.connectToRoom(withId: roomID, settings: ARDSettingsModel(), isLoopback: false)
        
        self.configureAudioSession()
        
        self.playRingTone()
    }
    
    // REOMVE ROOM
    // 通知Room Server把房間ID清空
    public func remove(_ roomID:String) {
        self.remove(roomID, {(status) in })
    }
    
    public func remove(_ roomID:String, _ callback:@escaping (Bool) -> Void) {
        
        let headers = [
            "Content-Type": "application/json",
            ]
        
        let parameters = [
            "os": "ios",
            ] as [String : Any]
        
        do {
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: WRConfig.shared.roomServerHost + "/remove/\(roomID)")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let error = error
                {
                    print(error)
                    
                    DispatchQueue.main.async {
                        callback(false)
                    }
                    
                    return
                }
                
                if  let data = data
                    ,  let resp = String(data: data, encoding: String.Encoding.utf8)
                {
                    DispatchQueue.main.async {
                        callback(true)
                    }
                }
            })
            
            dataTask.resume()
        } catch  {
            print("ERROR REMOVE ROOM: \(error.localizedDescription)")
            
            DispatchQueue.main.async {
                callback(false)
            }
        }
    }
    
    // SEND MESSAGE
    // 傳訊息給另一位使用者。
    // 若對方在APP內，可透過onMessage收到訊息
    public func send(userID:String, message:String)
    {

        let headers = [
            "Content-Type": "application/json",
        ]
        
        let parameters = [
            "type":"msg",
            "room_id": self.roomID,
            "sender_id": self.userID,
            "to": userID,
            "message": message
            ] as [String : Any]
        
        do {
            
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: WRConfig.shared.ApiHost + "/api/v1/message/send")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {

                } else {

                }
            })
            
            dataTask.resume()
        } catch  {
            print("ERROR SEMD MESSAGE \(error.localizedDescription)")
        }
        
    }
    
    // LEAVE ROOM
    // 離開房間
    // 關閉本地影像錄製，切斷WebRTC連線
    public func leave() {
        // 掛電話
        self.localVideoView.captureSession = nil
        self.captureController?.stopCapture()
        
        self.client.disconnect()
        self.roomID = ""
        self.onEvent(.leaveRoom)
    }
    
    // 開始鏡頭錄製
    public func openCamera()
    {
        if let captureController = self.captureController
        {
            captureController.startCapture()
        }
    }
    
    // 關閉鏡頭錄製
    public func closeCamera()
    {
        if let captureController = self.captureController
        {
            captureController.stopCapture()
        }
    }
    
    // 切換前後鏡頭
    public func switchCamera(){
        if let captureController = self.captureController
        {
            captureController.switchCamera()
        }
    }
    
    // 開啟麥克風錄製
    public func unMute(){
        if self.client != nil
            , self.client.localAudioTrack != nil
        {
            self.client.localAudioTrack.isEnabled = true
        }
    }
    
    // 關閉麥克風錄製
    public func mute(){
        if self.client != nil
            , self.client.localAudioTrack != nil
        {
            self.client.localAudioTrack.isEnabled = false
        }
    }
    
    private func configureAudioSession() {
        
        let config = RTCAudioSessionConfiguration()
        
        config.mode = AVAudioSessionModeVoiceChat
        
        config.category = AVAudioSessionCategoryPlayAndRecord
        
        config.categoryOptions = ((self.audioOutputType == .headphone)
                ? AVAudioSessionCategoryOptions.allowBluetooth
                : AVAudioSessionCategoryOptions(
                    rawValue: AVAudioSessionCategoryOptions.RawValue(
                        UInt8(AVAudioSessionCategoryOptions.allowBluetooth.rawValue) | UInt8(AVAudioSessionCategoryOptions.defaultToSpeaker.rawValue)
                )))
        
        let session = RTCAudioSession.sharedInstance()
        
        session.lockForConfiguration()
        
        do {
            
            if session.isActive
            {
                try session.setConfiguration(config)
            }
            else
            {
                try session.setConfiguration(config, active: true)
            }
        } catch {
            
            print ("Error configureAudioSession： \(error.localizedDescription))")
            
        }
        
        session.unlockForConfiguration()
        
    }
    
    private func playRingTone()
    {
        guard let path = WRConfig.shared.ringTonePath else {
            return
        }
        
        let file = URL(fileURLWithPath: path)
        
        do
        {
            let ap = try AVAudioPlayer(contentsOf: file)
            
            ap.numberOfLoops = -1
            
            ap.prepareToPlay()
            
            ap.play()
            _audioPlayer = ap
        }
        catch
        {
            print ("ERROR playRingTone:\(error.localizedDescription)")
        }
    }
    
    private func stopRingTone()
    {
        guard let player = _audioPlayer else {
            return
        }
        
        if player.isPlaying
        {
            player.stop()
        }
    }
}

extension WRClient: ARDAppClientDelegate {
    
    public func appClient(_ client: ARDAppClient!, didChange state: ARDAppClientState) {
        
        let stateString:String = [
            ARDAppClientState.connected.rawValue    : "connected",
            ARDAppClientState.connecting.rawValue   : "connecting",
            ARDAppClientState.disconnected.rawValue : "disconnected",
            ][state.rawValue] ?? "unknow"
        
        if state == .connected
        {
            self.onEvent(.joinedRoom)
        }
        if state == .disconnected
        {
            self.stopRingTone()
        }
    }
    
    public func appClient(_ client: ARDAppClient!, didChange state: RTCIceConnectionState) {
        
        let stateString = [
            RTCIceConnectionState.new.rawValue          :"NEW",
            RTCIceConnectionState.checking.rawValue     :"CHECKING",
            RTCIceConnectionState.connected.rawValue    :"CONNECTED",
            RTCIceConnectionState.completed.rawValue    :"COMPLETED",
            RTCIceConnectionState.failed.rawValue       :"FAILED",
            RTCIceConnectionState.disconnected.rawValue :"DISCONNECTED",
            RTCIceConnectionState.closed.rawValue       :"CLOSED",
            RTCIceConnectionState.count.rawValue        :"COUNT"
        ][state.rawValue] ?? "UNKNOW"
        
//        print ("RTCIceConnectionState didChange:\(stateString)")
        
    }
    private func configureVideoSession()
    {
        guard let localCapturer = self.localCapturer else {
            return
        }
        
        self.localVideoView.captureSession = localCapturer.captureSession
        
        let settings = ARDSettingsModel()
        
        let result = settings.storeVideoResolutionSetting("\(Int(self.localVideoSize.width))x\(Int(self.localVideoSize.height))")
        
        if self.captureController != nil
         , let captureController = self.captureController {
           captureController.stopCapture()
        }
        
        guard let captureController = ARDCaptureController(capturer: localCapturer, settings: settings) else {
            return
        }
        
        self.captureController = captureController
        
        if self.roomType == .video
        {
            captureController.startCapture()
            
            let format = captureController.captureFormat()
            
            let dimension = CMVideoFormatDescriptionGetDimensions(format!.formatDescription);
            
            let dimensionSize = CGSize(width: Int(dimension.width), height: Int(dimension.height))
            
            if __CGSizeEqualToSize(self.localVideoSize, dimensionSize) == false
            {
                self.localVideoSize = dimensionSize
                
                self.onEvent(.localVideoSizeChange)
            }
        }
    }
    public func appClient(_ client: ARDAppClient!, didCreateLocalCapturer localCapturer: RTCCameraVideoCapturer!) {
        self.localCapturer = localCapturer
        
        self.configureVideoSession()
        
    }
    private func selectFormatForDevice(_ device:AVCaptureDevice)
    {
//        NSArray<AVCaptureDeviceFormat *> *formats =
        //    [RTCCameraVideoCapturer supportedFormatsForDevice:device];
        //    int targetWidth = [_settings currentVideoResolutionWidthFromStore];
        //    int targetHeight = [_settings currentVideoResolutionHeightFromStore];
        //    AVCaptureDeviceFormat *selectedFormat = nil;
        //    int currentDiff = INT_MAX;
        let formats = RTCCameraVideoCapturer.supportedFormats(for: device)
        
        for format in formats
        {
            print("format", format)
//            CMVideoDimensions dimension = CMVideoFormatDescriptionGetDimensions(format.formatDescription);
            //    FourCharCode pixelFormat = CMFormatDescriptionGetMediaSubType(format.formatDescription);
            //    int diff = abs(targetWidth - dimension.width) + abs(targetHeight - dimension.height);
            //    if (diff < currentDiff) {
            //    selectedFormat = format;
            //    currentDiff = diff;
            //    } else if (diff == currentDiff && pixelFormat == [_capturer preferredOutputPixelFormat]) {
            //    selectedFormat = format;
            //    }
        }
        
        
//    return selectedFormat;
    }
    
    public func appClient(_ client: ARDAppClient!, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack!) {
        
    }
    public func appClient(_ client: ARDAppClient!, didReceiveRemoteDescription remoteDescription: RTCSessionDescription!) {
        
        self.stopRingTone()
        
        self.onEvent(.connected)
        
        self.configureAudioSession()
    }
    public func appClient(_ client: ARDAppClient!, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack!) {
        
        if self.remoteVideoTrack == remoteVideoTrack
        {
            return
        }
        
        self.remoteVideoTrack = remoteVideoTrack
        
    }
    
    public func appClient(_ client: ARDAppClient!, didGetStats stats: [Any]!) {
        
    }
    
    public func appClient(_ client: ARDAppClient!, didError error: Error!) {
        
        if let e = error as NSError?
         , e.domain == "ARDAppClient"
         , e.code   == -1 // kARDAppClientErrorRoomFull
        {
            self.leave()
        }
    }
    
    // 改走Laravel ECHO不使用
    public func appClient(_ client: ARDAppClient!, didReceiveMessage message: String!) {
        
        if let msg = message
        {
            //self.onMessage(msg)
        }
        
    }
    
    public func appClient(_ client: ARDAppClient!, onSignalingMessageOffer message: ARDSignalingMessage!) {
        
    }
    
    public func appClient(_ client: ARDAppClient!, onSignalingMessageAnswer message: ARDSignalingMessage!) {
        
    }
}

extension WRClient: RTCAudioSessionDelegate {
    public func audioSession(_ audioSession: RTCAudioSession, didDetectPlayoutGlitch totalNumberOfGlitches: Int64) {
        print ("AudioSession didDetectPlayoutGlitch, total: \(totalNumberOfGlitches)")
    }
    public func audioSessionDidStartPlayOrRecord(_ session: RTCAudioSession) {
        RTCDispatcher.dispatchAsync(on: .typeMain) {
            session.isAudioEnabled = true
        }
    }
    public func audioSessionDidStopPlayOrRecord(_ session: RTCAudioSession) {
        RTCDispatcher.dispatchAsync(on: .typeMain) {
            session.isAudioEnabled = false
        }
    }
}

extension WRClient:WRRemoteVideoViewDelegate{
    public func videoView(_ videoView: WRRemoteVideoView, didChangeVideoSize size: CGSize)
    {
        self.onEvent(.streamingSizeChange)
    }
}

extension WRClient:RTCVideoViewDelegate{
    public func videoView(_ videoView: RTCVideoRenderer, didChangeVideoSize size: CGSize) {
        self.remoteVideoView.layoutSubviews()
        self.onEvent(.streamingSizeChange)
    }
}
