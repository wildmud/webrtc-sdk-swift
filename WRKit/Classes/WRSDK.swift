//
//  WRSDK.swift
//  webrtc-demo
//
//  Created by Aleiku on 2018/10/24.
//  Copyright © 2018年 Institute for Information Industry. All rights reserved.
//

import Foundation
import PushKit

@objc public class WRSDK: NSObject {
    
    static public func launch() {
//        RTCSetMinDebugLogLevel(.verbose)
        RTCInitFieldTrialDictionary([String:String]())
        RTCInitializeSSL()
        RTCSetupInternalTracer()
    }
    
    static public func terminate() {
        RTCShutdownInternalTracer();
        RTCCleanupSSL();
    }
    
    static public func registerDeviceToken(_ userID:String, pushCredentials: PKPushCredentials) {
        if pushCredentials.type == PKPushType.voIP
        {
            let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
            
            NSLog("TOKEN:%@", token)
            
            WRSDK.registerDeviceToken(userID, token: token)
        }
    }
    
    static public func registerDeviceToken(_ userID:String, token: String) {
        let headers = [ "Content-Type": "application/json", ]
        
        let parameters = [
            "os": "ios",
            "token": token,
            ] as [String : Any]
        
        do {
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: WRConfig.shared.ApiHost + "/api/v1/user/\(userID)/device/token")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let error = error
                {
                    print(error)
                    return
                }
                
                if  let data = data
                    ,  let _ = String(data: data, encoding: String.Encoding.utf8)
                {
                    
                }
            })
            
            dataTask.resume()
        } catch  {
            print("ERROR registerDeviceToken: \(error.localizedDescription)")
        }
    }
    
    static public func removeDeviceToken(_ userID:String, pushCredentials: PKPushCredentials) {
        if pushCredentials.type == PKPushType.voIP
        {
            let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
            
            NSLog("TOKEN:%@", token)
            
            WRSDK.removeDeviceToken(userID, token: token)
        }
    }
    
    static public func removeDeviceToken(_ userID:String, token: String) {
        let headers = [ "Content-Type": "application/json", ]
        
        let parameters = [
            "os": "ios",
            "token": token,
            ] as [String : Any]
        
        do {
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: WRConfig.shared.ApiHost + "/api/v1/user/\(userID)/device/token")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "DELETE"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let error = error
                {
                    print(error)
                    return
                }
                
                if  let data = data
                    ,  let _ = String(data: data, encoding: String.Encoding.utf8)
                {
                    
                }
            })
            
            dataTask.resume()
        } catch  {
            print("ERROR registerDeviceToken: \(error.localizedDescription)")
        }
    }
    
    static public func notify(_ userID:String, params: [String : Any]) {
        
        let headers = [ "Content-Type": "application/json", ]
        
        do {
            let postData = try JSONSerialization.data(withJSONObject: params, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: WRConfig.shared.ApiHost + "/api/v1/user/\(userID)/notify")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let error = error
                {
                    print(error)
                    return
                }
                
                if  let data = data
                 ,  let _ = String(data: data, encoding: String.Encoding.utf8)
                {
                    
                }
            })
            
            dataTask.resume()
        } catch  {
            print("ERROR Notify: \(error.localizedDescription)")
        }
    }
}
