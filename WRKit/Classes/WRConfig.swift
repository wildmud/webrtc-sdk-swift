//
//  WRConfig.swift
//  WRKit
//
//  Created by Aleiku on 2018/10/21.
//  Copyright © 2018年 Wildmud Co., Ltd. All rights reserved.
//

open class WRConfig: NSObject {
    
    public static let shared = WRConfig()
    
    public var roomServerHost = "https://rtc.shoppalz.com"
    
    public var messageHost = "http://webrtc.shoppalz.com:6012"
    
    public var ApiHost = "http://webrtc.shoppalz.com"
    
    public var ringTonePath = Bundle.main.path(forResource: "Frameworks/WRKit.framework/ring", ofType: "mp3")
}
