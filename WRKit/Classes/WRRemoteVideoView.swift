//
//  WRRemoteVideoView.swift
//  webrtc-demo
//
//  Created by Aleiku on 2018/10/24.
//  Copyright © 2018年 Institute for Information Industry. All rights reserved.
//

import UIKit

public protocol WRRemoteVideoViewDelegate
{
    func videoView(_ videoView: WRRemoteVideoView, didChangeVideoSize size: CGSize)
}

open class WRRemoteVideoView: UIView {
    
    public var delegate:WRRemoteVideoViewDelegate?
    
    public var videoSize:CGSize = .zero
    
    var _videoView:UIView & RTCVideoRenderer = RTCEAGLVideoView(frame: .zero)
    {
        willSet{
            _videoView.removeFromSuperview()
        }
        didSet{
            self.addSubview(self._videoView)
        }
    }
    
    override init(frame: CGRect) {
        
        #if RTC_SUPPORTS_METAL
            _videoView = RTCMTLVideoView(frame: .zero)
        #else
            let vv = RTCEAGLVideoView(frame: .zero)
            _videoView = vv
        #endif
        
        super.init(frame: frame)
        
        if let vv = _videoView as? RTCEAGLVideoView
        {
            vv.delegate = self
        }
        
        self.addSubview(_videoView)
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func layoutSubviews() {
        let bounds = self.bounds
        
        self._videoView.frame = bounds
        
        if self.videoSize.width > 0 && self.videoSize.height > 0
        {
            var remoteVideoFrame = AVMakeRect(aspectRatio:self.videoSize , insideRect: bounds)
            
            var scale = CGFloat(1.0)
            if remoteVideoFrame.size.width  > 0
                , remoteVideoFrame.size.height > 0
            {
                scale = CGFloat(bounds.size.height / remoteVideoFrame.size.height)
            }
            else
            {
                scale = CGFloat(bounds.size.width / remoteVideoFrame.size.width)
            }
            remoteVideoFrame.size.height *= scale
            remoteVideoFrame.size.width  *= scale
            self._videoView.frame = remoteVideoFrame
            self._videoView.center = self.center
        } else {
            self._videoView.frame = bounds;
        }
    }
}

extension WRRemoteVideoView:RTCVideoRenderer {
    public func setSize(_ size: CGSize) {
        self.videoSize = size
        self._videoView.setSize(size)
        self.layoutSubviews()
    }

    public func renderFrame(_ frame: RTCVideoFrame?) {
        self._videoView.renderFrame(frame)
        self.layoutSubviews()
    }
}
extension WRRemoteVideoView:RTCVideoViewDelegate {
    public func videoView(_ videoView: RTCVideoRenderer, didChangeVideoSize size: CGSize) {
        print ("RTCVideoViewDelegate didChangeVideoSize:\(size.width) x \(size.height)")
        if __CGSizeEqualToSize(self.videoSize, size)
        {
            return
        }
        self.videoSize = size
        self.layoutSubviews()
        
        if let _delegate = self.delegate
        {
            _delegate.videoView(self, didChangeVideoSize: self.videoSize)
        }
    }
}
